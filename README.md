# Dart code generate
A demo to test Dart code generator

# Key Point
1. Dependences
   - source_gen: '0.9.4+4'
   - build_runner: ^1.6.7
   - code_builder: ^3.2.0  (optional)

2. Anotation Class
   - Pojo in lib/src/

3. Generator:
   - a class extend GeneratorForAnnotation<Pojo> in lib/src
   - return is file content generated

4. build.yaml:
   - builer.dart to point generator : Builder testBuilder(BuilderOptions options) => LibraryBuilder(TestGenerator());
   - build.yaml 
   ``` yaml
   builders:
       testBuilder:
        import: "package:dart_meta_test/builder.dart"
        builder_factories: ["testBuilder"]
        build_extensions: {".dart": [".g.part"]}
        auto_apply: root_package
        build_to: source
   ```
5. pub run build_runner build

