import 'package:build/build.dart';
import 'package:dart_meta_test/src/dart_meta_test_base.dart';
import 'package:source_gen/source_gen.dart';

Builder testBuilder(BuilderOptions options) =>
    LibraryBuilder(TestGenerator());
